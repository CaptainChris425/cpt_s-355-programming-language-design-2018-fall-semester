def conv(y):
	'''
	Tries to convert to an int
			then a float
			then checks if it is 
				True/False
			then an array by checking
				if the first and last are []
				then iterates the list
				appending the converted value
	or just returns the value found
	'''
	print('In convert with ' + str(y))
	x = y
	try:
		x = int(x)
		return x
	except ValueError:
		try:
			x = float(x)
		except ValueError:
			if x == 'False':
				x = False
				return x
			elif x == 'True':
				x = True
				return x
			
			if x[0] is '[' and x[-1] is ']':
				lst = []
				for c in x[1:-1].split(' '):
					lst.append(conv(c))
				return lst
			

	return x

lst = ['1','3','f','g','[1 True 3 4 5]']

lst = list(map(conv, lst))
print(lst)


hm = ' '
print(hm[0])
print(hm[-1])

print('[1 2 3 4]'[1:-1])
print('1 2 3 4'.split(' '))
