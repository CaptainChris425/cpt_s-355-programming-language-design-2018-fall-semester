import re

def tokenize(s):
	return re.findall(
#"/?[a-zA-Z][a-zA-Z0-9_]*|[[][a-zA-Z0-9_\s!][a-zA-Z0-9_\s!]*[]]|[-]?[0-9]+|[}{]+|%.*|[^ \t\n]"
#				,s)
"/?[a-zA-Z][a-zA-Z0-9_]*|[[][a-zA-Z0-9_\s!][a-zA-Z0-9_\s!]*[]]|[-]?[0-9]+|[}{]+|%.*|[^ \t\n]"
				,s)


def groupMatching(it):
	res = []
	for c in it:
		if c == '}':
			return res
		elif c == '{':
			res.append(groupMatching(it))
		else:
			res.append(c)
	return False	
	
def convlst(y):
	lst = []
	for i in y:
		lst.append(conv(i))
	return lst

def conv(y):
    '''
    Tries to convert to an int
            then a float
            then checks if it is 
                True/False
            then an array by checking
                if the first and last are []
                then iterates the list
                appending the converted value
    or just returns the value found
    '''
    print('In convert with ' + str(y))
    if type(y) == list:
        return convlst(y)

    x = y
    try:
        x = int(x)
        return x
    except ValueError:
        try:
            x = float(x)
        except ValueError:
            if x == 'False':
                x = False
                return x
            elif x == 'True':
                x = True
                return x
            if x[0] is '[' and x[-1] is ']':
                lst = []
                for c in x[1:-1].split(' '):
                    lst.append(conv(c))
                return lst


    return x


def parse(L):
	res = []
	it = iter(L)
	for c in it:
		if c == '}':
			return False
		elif c =='{':
			res.append(groupMatching(it))
		else: res.append(c)

	#print('res vv')
	#print(res)
	#print('res ^^')
	res = list(map(conv,res))

	return res

string = '[1 4 7 2 3 4 5] dup length /n exch def /fact { 0 dict begin { 1 } end } def n fact stack'
print(tokenize(string))
print(' ')
print(parse(tokenize(string)))

print(parse(['[1 2 3 4]','back','cat','{','True','{','a','}','[1 2 True False]','}','A']))

