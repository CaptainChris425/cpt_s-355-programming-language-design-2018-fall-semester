(*Christopher Young
09/05/2018
Homework 1 Standard ML*)

(*Question 1
	Making a function to check if a value exists in a list*)

fun exists(x,[]) = false
	| exists(x,z::rest) = if (x = z) then true else exists(x,rest);

(* ''a * ''a list) -> bool *)
(* It has this type because both the value and the values in the list
	must be comparable and also of the same type
	this is why they both have two '
	and are both alpha values *)

(* Question 2
	Make a function to count the occurances of a value in a list*)

fun countInList [] x = 0
	| countInList (v::rest) x = if v= x then 1+(countInList rest x ) else 0+(countInList rest x );


(*Question 3
	Make a function that will take a touple of two lists and return
	what is in the first list and not in the second*)

fun listDiff ([],y) = []
	| listDiff(x::rest,l) = if (countInList (x::rest) x)>(countInList l x) then x::listDiff(rest,l)else listDiff(rest,l); 

(* If the count of the items in the first list is greater than the count of the same item
	in the second list then it is a difference and should be appended onto the output*)

(*Question 4
	Write a function to output the first n elements in the list*)

fun firstN [] x = []
	| firstN (v::rest) x = if x=0 then [] else v::(firstN rest (x-1));

(* If the list is empty , the return should be empty list
	`for a list that is not empty
	pop off the head and append it to the output and
	call the function again with x-1
	if x = 0 then return
*)

(*Question 5
	Enter a name of a bus stop and the function will return
	the list of all the busses that go to that stop *)
val buses = [
	("Lentil",["Chinook","Orchard","Valley","Emerald","Providence","Stadium","Main","Arbor"
			,"Sunnyside","Fountain","Crestview","Wheatland","Walmart","Bishop","Derby","Dilke"]),
	("Wheat",["Chinook","Orchard","Valley","Maple","Aspen","TerreView","Clay","Dismores"
			,"Martin","Bishop","Walmart","PorchLight","Campus"]),
	("Silver",["TransferStation","PorchLight","Stadium","Bishop","Walmart","Shopco"
			,"RockeyWay"]),	
	("Blue",["TransferStation","State","Larry","TerreView","Grand","TacoBell","Chinook"
			,"Library"]),
	("Gray",["TransferStation","Wawawai","Main","Sunnyside","Crestview","CityHall","Stadium"
			,"Colorado"])
	]
fun first (x,_) = x;
fun second (_,x) = x;
fun busFinder s [] = []
	| busFinder s (v::rest) = if exists (s, second v) then (first v)::(busFinder s rest)
			else (busFinder s rest);

(*Why is the type ''a-> ('b * ''a list) list -> 'b list ? *)

(*
	This is the type because the parameters need to be a comparable alpha and then a list
		of touples where the second value is comparable. Since we never compare 
		with the first val in the touple, the fist parameter and second touple value
		need to be of the same comparable type. At the end the ouput is a 'b list
		because it is a list of the fist touple elements and does not need to be
		comparable or match the type of any other value in the function
*)

(*Question 6
	A function that takes in a list of resistors and returns the total resistance.*)
fun parallelResistors lst = 
	let
		fun parallelDenom [] = 0.0
			| parallelDenom (r::rest) = (1.0/r) + (parallelDenom rest);
	in
		1.0/(parallelDenom lst)
	end;

	

(*Question 7
	Two functions{
			pairNleft - takes a list and integer n and outputs a list of n-length lists where if there is a list with less than n elements it will be output first.
					i.e. pairNleft (2,[1,2,3,4,5]) = [[1],[2,3],[4,5]]
			pairNright = the same function but with the short list is output last
					i.e. pairNleft (2,[1,2,3,4,5]) = [[1,2],[3,4],[5]]
*)
fun pairRhelper n acc [] = [rev(acc)]
	| pairRhelper n acc (x::rest) = if (length(acc)) = n
					 then rev(acc)::(pairRhelper n (x::[]) rest)
		     			else (pairRhelper n (x::acc) rest);
fun pairLhelper n acc [] = [acc]
	| pairLhelper n acc (x::rest) = if (length(acc)) = n
					 then acc::(pairLhelper n (x::[]) rest)
		     			else (pairLhelper n (x::acc) rest);

fun pairNright (x,[]) = []
	|pairNright (n,lst) = pairRhelper n [] lst;

fun pairNleft (x,[]) = []
	|pairNleft (n,lst) =  rev(pairLhelper n [] (rev(lst)));

(*Test Functions*)
(*Exists Test*)

fun existsTest () = 
	let
		val exists1 = ((exists (1,[2,1,3])) = true)
		val exists2 = ((exists (5,[2,1,3])) = false)
		val exists3 = ((exists (1,[])) = false)
		val exists4 = ((exists ("c",["b","c","z"])) = true)
	in
		print ("exists: ------------------------\n" ^
			"  test1: " ^ Bool.toString(exists1) ^ "\n" ^	
			"  test1: " ^ Bool.toString(exists2) ^ "\n" ^	
			"  test1: " ^ Bool.toString(exists3) ^ "\n" ^	
			"  test1: " ^ Bool.toString(exists4) ^ "\n")
	end	

(*CountInList test*)
fun countInListTest () = 
	let
		val test1 = ((countInList [3,5,5,4,5,1] 5) = 3)
		val test2 = ((countInList [] 5) = 0)
		val test3 = ((countInList ["Hey","Hey","Hi","Hi"] "Hi") = 2)
		val test4 = ((countInList [true,false,false,false,true] true) = 2)
	in
		print ("exists: ------------------------\n" ^
			"  test1: " ^ Bool.toString(test1) ^ "\n" ^	
			"  test1: " ^ Bool.toString(test1) ^ "\n" ^	
			"  test1: " ^ Bool.toString(test1) ^ "\n" ^	
			"  test1: " ^ Bool.toString(test1) ^ "\n")
	end	

(*listDiff test*)
fun listDiffTest () = 
	let
		val test1 = ((listDiff (["a","b","c"],["b"])) = ["a","c"]) 
		val test2 = ((listDiff ([1,2,3],[1,1,2])) = [3]) 
		val test3 = ((listDiff ([[2,3],[1,2],[2,3]],[[1],[2,3]])) = [[2,3],[1,2]])
		val test4 = ((listDiff ([1,2,3],[])) = [1,2,3])
	in
		print ("exists: ------------------------\n" ^
			"  test1: " ^ Bool.toString(test1) ^ "\n" ^	
			"  test1: " ^ Bool.toString(test1) ^ "\n" ^	
			"  test1: " ^ Bool.toString(test1) ^ "\n" ^	
			"  test1: " ^ Bool.toString(test1) ^ "\n")
	end	

(*firstN test*)
fun firstNTest () = 
	let
		val test1 = ((firstN ["a","b","c","x","y"] 3) = ["a","b","c"]) 
		val test2 = ((firstN [1,2,3,4,5,6,7] 4) = [1,2,3,4]) 
		val test3 = ((firstN [[1,2,3],[4,5],[6],[],[7,8],[]] 4) = [[1,2,3],[4,5],[6],[],[7,8],[]]) 
		val test4 = ((firstN [] 3) = []) 
	in
		print ("exists: ------------------------\n" ^
			"  test1: " ^ Bool.toString(test1) ^ "\n" ^	
			"  test1: " ^ Bool.toString(test1) ^ "\n" ^	
			"  test1: " ^ Bool.toString(test1) ^ "\n" ^	
			"  test1: " ^ Bool.toString(test1) ^ "\n")
	end	

(*busFinder test*)
fun busFinderTest () =
	let
		val test1 = ((busFinder "Walmart" buses) = ["Lentil","Wheat","Silver"]) 
		val test2 = ((busFinder "Shopco" buses) = ["Silver"]) 
		val test3 = ((busFinder "Main" buses) = ["Lentil","Gray"]) 
		val test4 = ((busFinder "Beasley" buses) = []) 
	in
		print ("exists: ------------------------\n" ^
			"  test1: " ^ Bool.toString(test1) ^ "\n" ^	
			"  test1: " ^ Bool.toString(test1) ^ "\n" ^	
			"  test1: " ^ Bool.toString(test1) ^ "\n" ^	
			"  test1: " ^ Bool.toString(test1) ^ "\n")
	end	

(*parallelResistors test*)
fun parallelResistorsTest () =
	let
		val test1 = Real.==((parallelResistors [10.0,10.0,10.0,10.0]),2.5)
		val test2 = Real.==((parallelResistors [8.0,16.0,4.0,16.0]),2.0)
		val test3 = Real.==((parallelResistors [5.0,10.0,2.0]),1.25)
		val test4 = Real.==((parallelResistors [0.5,0.5]),0.25)

	in
		print ("exists: ------------------------\n" ^
			"  test1: " ^ Bool.toString(test1) ^ "\n" ^	
			"  test1: " ^ Bool.toString(test1) ^ "\n" ^	
			"  test1: " ^ Bool.toString(test1) ^ "\n" ^	
			"  test1: " ^ Bool.toString(test1) ^ "\n")
	end


(*pairN test*)

fun pairNTest () = 
	let
		val test1 = (pairNleft (2,[1,2,3,4,5]) = [[1],[2,3],[4,5]])
		val test2 = (pairNright (2,[1,2,3,4,5]) = [[1,2],[3,4],[5]])
		val test3 = (pairNleft (3,[1,2,3,4,5]) = [[1,2],[3,4,5]])
		val test4 = (pairNleft (3,[1,2,3,4,5]) = [[1,2,3],[4,5]])
	in
		print ("exists: ------------------------\n" ^
			"  test1: " ^ Bool.toString(test1) ^ "\n" ^	
			"  test1: " ^ Bool.toString(test1) ^ "\n" ^	
			"  test1: " ^ Bool.toString(test1) ^ "\n" ^	
			"  test1: " ^ Bool.toString(test1) ^ "\n")
	end	
(*
existsTest ();
countInListTest ();
listDiffTest ();
firstNTest ();
busFinderTest ();
parallelResistorsTest ();
pairNTest ();
*)
