#!usr/bin/python

#Christopher Young
#Homework 3 python warm-up

import random

def addDict(d):
	ret = {}
	for k in d.keys():
		for day in d[k].keys():
			if day not in ret.keys(): ret[day] = 0
			ret[day] +=	d[k][day] 
	return ret

def testaddDict():
	print("Testing addDict function:")
	if addDict({'355':{'Mon':3,'Wed':2,'Sat':2},'360':{'Mon':3,'Tue':2,'Wed':2,'Fri':10},'321':{'Tue':2,'Wed':2,'Thu':3},'322':{'Tue':1,'Thu':5,'Sat':2}}) != {'Wed':6,'Fri':10,'Tue':5,'Mon':6,'Thu':8,'Sat':4}:
		return False
	if addDict({'355':{'Mon':0,'Wed':0,'Sat':0},'360':{'Mon':1,'Tue':1,'Wed':1,'Fri':1},'321':{'Tue':0,'Wed':0,'Thu':0},'322':{'Tue':0,'Thu':0,'Sat':0}}) != {'Wed':1,'Fri':1,'Tue':1,'Mon':1,'Thu':0,'Sat':0}:
		return False
	return True


def addDicts(d1,d2):
	ret = {}
	for k in d1.keys():
		ret[k] = d1[k]
	for k in d2.keys():
		if k not in ret.keys(): ret[k] = d2[k]
		else: ret[k] += d2[k]
	return ret

def addDictN(L):
	ret = map(addDict,L)
	ret = reduce(addDicts,ret)
	return ret

def testaddDictN():
	print("Testing addDictN function:")
	days = {'355':{'Mon':3,'Wed':2,'Sat':2},'360':{'Mon':3,'Tue':2,'Wed':2,'Fri':10},'321':{'Tue':2,'Wed':2,'Thu':3},'322':{'Tue':1,'Thu':5,'Sat':2}}
	if (addDictN([{'x':{'A':5,'B':6,'C':7}},{'x':{'A':5,'B':6,'C':7}}]) != {'A':10,'C':14,'B':12}):
		return False
	daylist = [days,days,days,days]
	if (addDictN(daylist) != {'Wed':24,'Thu':32,'Tue':20,'Mon':24,'Fri':40,'Sat':16}):
		return False
	return True
		

def lookupVal(L,k):
	for i in L[::-1]:
		if k in i.keys(): return i[k]

def testlookupVal():
	print("Testing lookupVal function:")
	L1 = [{'x':1,'y':True,'z':'found'},{'x':2},{'y':False}]
	if (lookupVal(L1,'t') != None): return False
	if (lookupVal(L1,'x') != 2): return False
	if (lookupVal(L1,'y') != False): return False
	return True


def lookupVal2(tL,k):
	return lv2helper(tL,k,tL[-1][0])	

def lv2helper(L,k,i):
	x = lookupVal([L[i][1]],k)
	if x is not None: return x
	else: return lv2helper(L,k,L[i][0])
	
def testlookupVal2():
	print("Testing lookupVal2 function:")
	L2 = [(0,{'x':0,'y':True,'z':'zero'}),(0,{'x':1}),(1,{'y':False}),(1,{'x':3,'z':'three'}),(2,{})]
	if (lookupVal2(L2,'x') != 1): return False
	if (lookupVal2(L2,'y') != False): return False
	if (lookupVal2(L2,'z') != 'zero'): return False
	return True


def numPaths(m,n,blocks):
	return makemove((1,1),m,n,blocks)

def makemove(spot,m,n,blocks):
	if spot in blocks: return 0
	moves = 0
	if spot[0] == m and spot[1] == n:
		return 1
	if not spot[0]+1 > m:
		moves += makemove((spot[0]+1,spot[1]),m,n,blocks)
	if not spot[1]+1 > m:
		moves += makemove((spot[0],spot[1]+1),m,n,blocks)	
	return moves

def testnumPaths():
	print("Testing numPaths function:")
	if (numPaths(2,2,[(2,1)]) != 1): return False
	if (numPaths(3,3,[(2,3)]) != 3): return False
	if (numPaths(4,3,[(2,2)]) != 4): return False
	if (numPaths(10,3,[(2,2),(7,1)]) != 27): return False
	return True

def palin(s):
	return s == s[::-1]

def palindromes(s):
	ret = []
	for i in range(0,len(s)-1):
		if i < len(s)-1:
			if palin(s[i:i+1]):
				ret.append(s[i:i+1])
		if i > 0:
			for j in range(0,i):
				if j+i < len(s):
					if palin(s[i-j:i+j]):
						ret.append(s[i-j:i+j])
	return ret


'''
	ret = []
	print('length of s -> %d' %(len(s)))
	for i in range(0,len(s)-1):
		print('for loop s[%d] = %c == s[%d] = %c' %(i,s[i],i+1,s[i+1]))
		if (s[i] == s[i+1]):
			ret.append(s[i::i+1])
		if i > 0 and i < len(s)-1:
			j=1
			while((i+j)<=len(s)-1 and s[i-j] == s[i+j] and j<=i):
				
				print('while loop s[%d] = %c == s[%d] = %c' %(i-j,s[i-j],i+j,s[i+j]))
				ret.append(s[i-j::i+j])
				j+=1
	return ret
'''


class iterApply():
	def __init__(self,n,f):
		self.curr = n
		self.fun = f

	def __next__(self):
		result = self.fun(self.curr)
		self.curr += 1
		return result
	def __prev__(self):
		self.curr -= 1
	
	def __iter__(self):
		return self

def iMerge(f1,f2,n):
	ret = []
	for i in range(0,n):
		v1 = f1.__next__()
		v2 = f2.__next__()
		if v1 < v2:
			ret.append(v1)
			f2.__prev__()
		else:
			ret.append(v2)
			f1.__prev__()
	return ret

class Stream(object):
	def __init__(self, first, compute_rest, empty = False):
		self.first = first
		self._compute_rest = compute_rest
		self.empty = empty
		self._rest = None
		self._computed = False

	@property
	def rest(self):
		assert not self.empty, 'Empty streams have no rest.'
		if not self._computed:
			self._rest = self._compute_rest()
			self._computed = True
		return self._rest

def streamRandoms(k,minn,maxx):
	def compute_rest():
		return streamRandoms(random.randint(minn,maxx),minn,maxx) 
	return Stream(k,compute_rest)

def oddStream(stream):
	if stream.empty:
		return stream
	def compute_rest():
		return oddStream(stream.rest)	
	while stream.first%2 == 0:
	 	stream = stream.rest
	return Stream(stream.first, compute_rest)





if __name__ == '__main__':
		print(testaddDict())
		print(testaddDictN())
		print(testlookupVal())
		print(testlookupVal2())
		print(testnumPaths())
		squares = iterApply(1,lambda x: x**2)
		print(squares.__next__())
		print(squares.__next__())
		print(squares.__next__())
		triples = iterApply(1,lambda x: x**3)
		squares = iterApply(1,lambda x: x**2)
		print(iMerge(squares,triples,8))
		print(iMerge(squares,triples,10))
		N =	streamRandoms(1,1,100) 
		print(N.first)
		N = N.rest 
		print(N.first)
		N = N.rest 
		print(N.first)
		N = N.rest 
		print(N.first)
		N = N.rest 
		print(N.first)

		print('Done with rand')
		N = oddStream(N)
		print(N.first)
		N = N.rest 
		print(N.first)
		N = N.rest 
		print(N.first)
		N = N.rest 
		print(N.first)
		N = N.rest 
		print(N.first)

		print(palindromes('cabbbaccab'))


