import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
//import java.io.Console; //Enable this if you would like to print on the console for debugging

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Board extends JPanel 
        implements ActionListener {
	/*define constants for customizing the game */
	public  final int BOARD_WIDTH = 10; //number of rows in the board
	public  final int BOARD_HEIGHT = 20; //number of columns
	public final int CELL_WIDTH = 30;
	public final int CELL_HEIGHT = 35;	
    public  final char SPACE = ' ';
    public  final String TARGET = "355";
    
    private final int DELAY = 400;
    private static final Color BLACK =  new Color(0,0,0);
    
    private Timer timer;  //We use the java.util.Timer to create a game cycle. The shapes move on a square by square basis (not pixel by pixel).
    private boolean isFallingFinished = false; //isFallingFinished is set to true when the fall of a piece is completed; it is set to false when the next piece starts to fall. 
    private boolean isStarted = false;  //isStarted is set to false when the board height reaches the limit and the next piece can't be moved (i.e., game is over). The program exist when isStarted becomes false. 
    private boolean isPaused = false;   //set to true when the game is paused; false otherwise.
    private JLabel statusbar; 
    private JLabel scorebar;  //The label displaying the game score
    private JLabel scoretext; 
    private Font textFont;
    private Piece curPiece;   //The current (falling) piece
    private char [][] board; //The board grid
    private Player player;

    
    public Board(Tetris parent) {

        initBoard(parent);
    }
    
    /*clear all cells on the board. A cell in the board is considered empty if the character at that cell is SPACE. Iniatially all cells are initialized with SPACE.*/
    private void clearBoard() {
        for (int i = 0; i < BOARD_HEIGHT; i++) {
        	for (int j = 0; j < BOARD_WIDTH; j++) {
        		board[j][i] = SPACE;
        	}
        }
    }
    
    /*calculate the cell width and height in pixels*/
    private int squareWidth() { return (int) getSize().getWidth() / BOARD_WIDTH; }
    private int squareHeight() { return (int) getSize().getHeight() / BOARD_HEIGHT; }
    
    /*calculate the X coordinate of the left upper corner of j th cell on a row. (j is a 0-based index)*/  
    private int calcX(int j) {
    	return (j * squareWidth());
        
    }
    /*calculate the Y coordinate of the left upper corner of i th cell on a column. (i is a 0-based index)*/  
    private int calcY(int i) {
        Dimension size = getSize();
    	int boardTop = (int) size.getHeight() - BOARD_HEIGHT * squareHeight(); 
    	return (boardTop + i * squareHeight());
        
    }
    /*initialize the game board, create the first piece, and start the timer */
    private void initBoard(Tetris parent) {
        
       setFocusable(true);
       curPiece = new Piece();
       player = new Player();
       //create the Timer. The board object is registered as the listener  for the timer. When the timer is fired, Board's action listener method will be called. 
       timer = new Timer(DELAY, this);
       timer.start(); 

   	   this.textFont = new Font("Helvatica", Font.BOLD, 22);
       statusbar = parent.getStatusBar(); //get the reference to the Tetris's status bar
       scorebar = parent.getScoreBar();   //get the reference to the Tetris's score bar
       scoretext = parent.getScoreText();   //get the reference to the Tetris's score bar
       
       scoretext.setFont(textFont);
       scorebar.setBorder(BorderFactory.createEtchedBorder(1));
       scorebar.setFont(textFont);
       statusbar.setFont(textFont);
       //statusbar.setText("Woah this wont work");
       
       addKeyListener(new TAdapter());
       //create and clear the board
       board = new char [BOARD_WIDTH] [BOARD_HEIGHT];
       clearBoard();    
       
    }

    /* When Timer is fired, Board's override of actionPerformed will be called*/
    @Override
    public void actionPerformed(ActionEvent e) {
        /*if the falling of the current piece is completed, create a new piece*/
        if (isFallingFinished) {        
            isFallingFinished = false;
            createNewPiece();
        } else{ // else move the current piece one level down on the board.     
            oneLineDown();
        }
    }

/* Start the game. */  
    public void start()  {
        
        if (isPaused)
            return;

        isStarted = true;
        isFallingFinished = false;
        
        createNewPiece();
        timer.start();
        clearBoard();
    }
    /* Pause the game. */  
    private void pause()  {
        
        if (!isStarted)
            return;

        isPaused = !isPaused;
        
        if (isPaused) {
            
            timer.stop();
            statusbar.setText("PAUSED");
        } else {
            
            timer.start();
            statusbar.setText(" ");
        }
        
        repaint();
    }

    /* draw a square over the non-empty board cells (a cell is non-empty if its value is not SPACE). The color of the cell is determined based on the cell value. */
    private void drawSquare(Graphics g, int x, int y, char number)  {
        
        Color colors[] = { new Color(50, 150, 50), new Color(204, 102, 102), 
            new Color(102, 204, 102), new Color(102, 102, 204), 
            new Color(204, 204, 102), new Color(204, 102, 204), 
            new Color(0, 255, 0), new Color(0, 0, 255),
            new Color(102, 204, 204), new Color(218, 170, 0)
        };

        Color color = colors[Character.getNumericValue(number)];

        g.setColor(color);
        g.fillRect(x + 1, y + 1, squareWidth() - 2, squareHeight() - 2);
        g.setColor(BLACK);
        g.setFont(textFont);
        g.drawString(String.valueOf(number), x+(squareWidth()-(CELL_WIDTH/2)+1)/2, y+(squareHeight()+(CELL_HEIGHT/2))/2 );
             
        g.setColor(color.brighter());
        g.drawLine(x, y + squareHeight() - 1, x, y);
        g.drawLine(x, y, x + squareWidth() - 1, y);

        g.setColor(color.darker());
        g.drawLine(x + 1, y + squareHeight() - 1,
                         x + squareWidth() - 1, y + squareHeight() - 1);
        g.drawLine(x + squareWidth() - 1, y + squareHeight() - 1,
                         x + squareWidth() - 1, y + 1);

    }

    /*draw the squares for the non empty cells on the board (already dropped pieces and the current piece currently falling)*/
    private void doDrawing(Graphics g) {       
   	
    	/*draw the squares for already dropped pieces*/
        for (int i = 0; i < BOARD_HEIGHT; i++) {
            for (int j = 0; j < BOARD_WIDTH; j++) {
                if (board[j][i] != SPACE) {
                	drawSquare(g, calcX(j),calcY(i), board[j][i]);
                }
            }
        }
        /*draw the square for the currently dropping piece*/
        if (curPiece.getNumber() != SPACE) {                
                drawSquare(g, calcX(curPiece.getX()),calcY(curPiece.getY()), curPiece.getNumber());
            
        }        
    }

    /*Board's overwrite for paintComponent. This will be called every time "repaint() is called . */
    @Override
    public void paintComponent(Graphics g) { 

        super.paintComponent(g);
        doDrawing(g);
    }
    
    public int checkPatterns(int x, int y, char number, int count, int direction) {
    	//Recursively checks all cells around a starting cell for 3 adjacent cells with the same number
    	//Once it finds 3 it will then call the remove all equal function to remove all the cells
    	if (count >= 3) { //If three adjacent cells were found
    		removeAllEqualNodes(x, y, number); //remove all adjacent matching cells
    		return count;
    	}
    	if(x < 9 && direction != 1) { //If it can go right and didnt come from right
	    	if(board[x+1][y] == number) {
	    		count = checkPatterns(x+1, y, number, count+1, 0); //move and increment count
	    	}
    	}
    	if (x>0 && direction != 0) { //if it can go left and didnt come from left
	    	if(board[x-1][y] == number) {
	    		count = checkPatterns(x-1, y, number, count+1, 1); //move and increment count
	    	}
    	}
    	if (y < 19 && direction != 3) { //if it can go down and didnt come from down
	    	if(board[x][y+1] == number) {
	    		count = checkPatterns(x, y+1, number, count+1, 2); //move and increment count
	    	}
    	}
    	if (y>0 && direction != 2) { //if it can go up and didnt come from up
	    	if(board[x][y-1] == number) {
	    		count = checkPatterns(x, y-1, number, count+1, 3); //move and increment count
	    	}
    	}
    	
    	return count;

    }
    
    public int checkTargetString(int x, int y) {
    	/*Everything in the function is in the form:
    	 * 
    	 * If it is a 3
    	 * 		*If it can check for 2 5s left up right and down
    	 * 		*If it found them remove them and return 1
    	 * 
    	 * If it is a 5
    	 * 		*If it can check left up right and down for a 5 followed by a 3
    	 * 		*If they are found, remove them and return 1
    	 * 		
    	 * 		*If not:
    	 * 		*Check left for a 3 and right for a 5 or left for a 5 and right for a 3
    	 * 		*Check up for a 3 and down for a 5 or up for a 5 and down for a 3
    	 * 		*If found, remove them and return 1
    	 * 
    	 * If none were found return 0
    	 */
    	if (board[x][y] == '3') {
    		if (y < 18) { //Can only check downwards if this is true
    			if (board[x][y+1] == '5' && board[x][y+2] == '5') {
    				board[x][y] = SPACE;
    				board[x][y+1] = SPACE;
    				board[x][y+2] = SPACE;
    				return 1;
    			}
    		}
    		if (y > 1) {//Can only check upwards if this is true
    			if (board[x][y-1] == '5' && board[x][y-2] == '5') {
    				board[x][y] = SPACE;
    				board[x][y-1] = SPACE;
    				board[x][y-2] = SPACE;
    				return 1;
    			}
    		}
    		if (x < 8) {//Can only check right if this is true
    			if (board[x+1][y] == '5' && board[x+2][y] == '5') {
    				board[x][y] = SPACE;
    				board[x+1][y] = SPACE;
    				board[x+2][y] = SPACE;
    				return 1;
    			}
    		}
    		if (x > 1) {//Can only check left if this is true
    			if (board[x-1][y] == '5' && board[x-2][y] == '5') {
    				board[x][y] = SPACE;
    				board[x-1][y] = SPACE;
    				board[x-2][y] = SPACE;
    				return 1;
    			}
    		}
    		
    		
    	}else if (board[x][y] == '5') { //All this logic is if the peice is a 5
    		if (y < 18) {
    			if (board[x][y+1] == '5' && board[x][y+2] == '3') {
    				board[x][y] = SPACE;
    				board[x][y+1] = SPACE;
    				board[x][y+2] = SPACE;
    				return 1;
    			}else if (y>0) {
	    			if (board[x][y-1] == '5' && board[x][y+1] == '3') {
	    				board[x][y] = SPACE;
	    				board[x][y-1] = SPACE;
	    				board[x][y+1] = SPACE;
	    				return 1;
	    			}
    			}
    		}
    		if (y > 1) {
    			if (board[x][y-1] == '5' && board[x][y-2] == '3') {
    				board[x][y] = SPACE;
    				board[x][y-1] = SPACE;
    				board[x][y-2] = SPACE;
    				return 1;
    			}else if (y<19) {
	    			if (board[x][y-1] == '3' && board[x][y+1] == '5') {
	    				board[x][y] = SPACE;
	    				board[x][y-1] = SPACE;
	    				board[x][y+1] = SPACE;
	    				return 1;
	    			}
    			}
    		}
    		if (x < 8) {
    			if (board[x+1][y] == '5' && board[x+2][y] == '3') {
    				board[x][y] = SPACE;
    				board[x+1][y] = SPACE;
    				board[x+2][y] = SPACE;
    				return 1;
    			}else if (x > 0) {
    				if (board[x-1][y] == '3' && board[x+1][y] == '5') {
    					board[x][y] = SPACE;
        				board[x-1][y] = SPACE;
        				board[x+1][y] = SPACE;
        				return 1;
    				}
    			}
    		}
    		if (x > 1) {
    			if (board[x-1][y] == '5' && board[x-2][y] == '3') {
    				board[x][y] = SPACE;
    				board[x-1][y] = SPACE;
    				board[x-2][y] = SPACE;
    				return 1;
    			}else if (x < 9) {
    				if (board[x-1][y] == '5' && board[x+1][y] == '3') {
    					board[x][y] = SPACE;
        				board[x-1][y] = SPACE;
        				board[x+1][y] = SPACE;
        				return 1;
    				}
    			}
    		}
    		
    		
    	}
    	return 0;
    	
    	
    }
    
    
    /*move the current piece all the way down on the board*/
    private void dropDown() {
        
        int newY = curPiece.getY();
        
        while (newY < BOARD_HEIGHT-1) {
            
            if (!tryMove(curPiece, 0 , 1))
                break;
            ++newY;
        }
        
        pieceDropped();
    }
   
    /*move the current piece one row down on the board*/
    private void oneLineDown()  {
        
        if (!tryMove(curPiece, 0, 1))
            pieceDropped();
    }

    public void dropAndRemoveAll() {
    	//This function acts as the gravity for when cells below get removed
    	//It will check in each x column all the way up
    	//and it will move each piece it finds down the appropriate amount.
    	boolean moved = false;
    	for(int x = 0; x <10; x++) {
    		for (int y = 18; y > -1; y--) {
    			int y2 = y;
    			if (board[x][y] != SPACE) {
	    			while(board[x][y2+1] == SPACE) {
	    				//Moving the piece down until the space below it is not a SPACE
	    				moved = true;
	    				char number = board[x][y2];
		        		board[x][y2] = SPACE;
		        		board[x][y2+1] = number;
		        		y2++;
		        		//if we are at the bottom it is dropped
		        		if (y2 == 19) break;
	    			}
    			}
    		}
    		
    		
    	}
    	//If any piece had been moved we have to check all the patterns
    	if (moved) {
    		//trying to update the board after a drop
    		repaint();
    		try
        	{
        	    Thread.sleep(500);
        	}
        	catch(InterruptedException ex)
        	{
        	    Thread.currentThread().interrupt();
        	}
    		//For all board pieces
    		for(int x = 0; x <10; x++) {
        		for (int y = 19; y > -1; y--) {
        			if (board[x][y] != SPACE) { //if it is not empty
        				if(checkTargetString(x,y) == 0) {	//check for target string
        					checkPatterns(x, y, board[x][y], 1, 5); //string not found check for matches
        				}else {
        					//Add to player score
        					player.addNumRemoved(3);
        					player.addScore(20);
        					
        				}
        				
	        			dropAndRemoveAll(); //Drop all peices again and check for target or matches
        			}
        		}
    		}
    	}

    	
    }
    /*checks if the fall of the current piece is completed. 
     * **TODO** If so, updates the board and the score.*/
    private void pieceDropped() {                
        updateBoard(curPiece); //Finds ways to remove cells
        dropAndRemoveAll(); //Drops all nodes and then checks for patterns and target string 
        //Update status and score bars
        statusbar.setText(Integer.toString(player.getNumRemoved()));
		scorebar.setText(Integer.toString(player.getScore()));
        if (!isFallingFinished)
            createNewPiece();
    }

    /*creates the new piece; if the board is full (i.e, can't move the current piece) stops the game. */  
    private void createNewPiece()  {
        
        curPiece.setRandomNumber();
        curPiece.setX(BOARD_WIDTH / 2 + 1);
        curPiece.setY(0);

        if (!tryMove(curPiece, 0, 0)) {
            
            curPiece.setNumber(SPACE);
            timer.stop();
            isStarted = false;
            statusbar.setText("GAME OVER");
        }
    }

    /*moves the current piece by newX,newY on the Board and repaints the Board*/
    private boolean tryMove(Piece newPiece, int newX, int newY) {
        int x = newPiece.getX() + newX;
        int y = newPiece.getY() + newY;
           
        if (x < 0 || x >= BOARD_WIDTH || y < 0 || y >= BOARD_HEIGHT)
        	return false;     
        
        if (board[x][y] != SPACE)
            return false;
       

        curPiece = newPiece;
        curPiece.setX(x);
        curPiece.setY(y);

        repaint();

        return true;
    }
    
    /*Updates the Board: 
     * clears all cell groups including 3 (or more) matching adjacent cells
     * clears all cell groups including the target characters 
     *     - can appear either horizontal(left to right or right to left )  or vertical (up to down or down to up)
     *     - the characters of the target string need to appear in the same order.*/
    public void updateBoard(Piece curPiece) {
        
    	board[curPiece.getX()][curPiece.getY()] = curPiece.getNumber();
    	/*TODO*/
        /*Update the Board: 
         * clear all cell groups including 3 (or more) matching adjacent cells
         * clear all cell groups including the target characters */
    	if(checkTargetString(curPiece.getX(),curPiece.getY()) == 0) { //If it found the target string this will return 1
    		checkPatterns(curPiece.getX(),curPiece.getY(),curPiece.getNumber(), 1, 5); //Now check for adjacent cells
    	}else {//Found the target string
    		//Add to player score
    		player.addNumRemoved(3); 
			player.addScore(20);
		}
    	//Update status and score bars
    	statusbar.setText(Integer.toString(player.getNumRemoved())); 
    	scorebar.setText(Integer.toString(player.getScore()));
    	isFallingFinished = false;
    	

     }

    public int removeAllEqualNodes(int x, int y, char number) {
    	//This function is run when three cells of equal number are found
    	//it will recursively change all adjacent and equal cells to SPACE
    	board[x][y] = SPACE;
    	player.addNumRemoved(1);
    	player.addScore(5);
    	if(x < 9) {//If it can go right go right
	    	if(board[x+1][y] == number) {
	    		removeAllEqualNodes(x+1, y,number);
	    	}
    	}
    	if (x>0) {//If it can go left go left
	    	if(board[x-1][y] == number) {
	    		removeAllEqualNodes(x-1, y, number);
	    	}
    	}
    	if (y < 19) {//If it can go down go down
	    	if(board[x][y+1] == number) {
	    		removeAllEqualNodes(x, y+1, number);
	    	}
    	}
    	if (y>0) {//if it can go up go up
	    	if(board[x][y-1] == number) {
	    		removeAllEqualNodes(x, y-1, number);
	    	}
    	}
    	return 0;
    }
    
    

    /* handles the key presses*/
    class TAdapter extends KeyAdapter {
        
         @Override
         public void keyPressed(KeyEvent e) {

             if (!isStarted || curPiece.getNumber() == SPACE) {  
                 return;
             }

             int keycode = e.getKeyCode();

             if (keycode == 'P') {
                 pause();
                 return;
             }

             if (isPaused)
                 return;

             switch (keycode) {
                 
             case KeyEvent.VK_LEFT:
                 tryMove(curPiece, - 1, 0);
                 break;
                 
             case KeyEvent.VK_RIGHT:
                 tryMove(curPiece,  1, 0);
                 break;
                 
             case KeyEvent.VK_DOWN:
            	 tryMove(curPiece, 0, 2);
                 break;
                 
             case KeyEvent.VK_UP:
                 tryMove(curPiece, 0, 0);
                 break;
                 
             case KeyEvent.VK_SPACE:
                 dropDown();
                 break;
                 
             case 'D':
                 oneLineDown();
                 break;
             }
         }
     }
}
