
public class Player {
	private int score = 0;
	private int numRemoved = 0;
	
	public Player() {
		setScore(0);
		setNumRemoved(0);
	}
	public void setScore(int newScore) {
		this.score = newScore;
	}
	public void setNumRemoved(int newRemoved) {
		this.numRemoved = newRemoved;
	}
	public void addScore(int newScore) {
		this.score += newScore;
	}
	public void addNumRemoved(int newRemoved) {
		this.numRemoved += newRemoved;
	}
	public int getScore() {
		return this.score;
	}
	public int getNumRemoved() {
		return this.numRemoved;
	}
}
