'''
Christopher Young
Python interpreter for Postscript like language
CptS 355
'''
import sys
import re
debugFlag = False#True#
currentDictionary = 0 
currentScope = 'dynamic'
linksdict = {}
def debug(s):
	if debugFlag:
		print(s)

def quitter():
	debug('in quitter')
	sys.exit(0)

#------------------------- 10% -------------------------------------   
# The operand stack: define the operand stack and its operations   
opstack = []   
# now define functions to push and pop values to the top of to/from the top of 
#the stack (end of the list). Recall that `pass` in Python is a space 
#holder: replace it with your code.   
def opPop():    
	'''pops the top value off if there is atleast one value'''
	if len(opstack) < 1:
		print("No values to pop")
		return
	return opstack.pop(0)

def opPush(value):   
	'''Pushes the value into the top of the stack'''
	opstack.insert(0,value)

# Remember that there is a Postscript operator called "pop" so we choose       
#different names for these functions.   
#-------------------------- 20% -------------------------------------   
# The dictionary stack: define the dictionary stack and its operations   
dictstack = [({},0)]   
# now define functions to push and pop dictionaries on the dictstack, to define
# name, and to lookup a name   
def dictPop():   
	'''pops the top dictionary off the stack'''
	return dictstack.pop(currentDictionary)

# dictPop pops the top dictionary from the dictionary stack. 
def dictPush(dic,staticlink):
	'''Pushes a dictionary onto the stack either passed in or an empty one is created'''
	if dic is not None:
		dictstack.append((dic,staticlink))
	else:
		dictstack.append(({},staticlink))

#dictPush pushes a new dictionary to the dictstack. Note that, your interpreter 
#will call dictPush only when Postscript "begin" operator is called. "begin" 
#should pop the empty dictionary from the opstack and push it onto the dictstack 
#by calling dictPush. You may either pass this dictionary (which you popped from 
#opstack) to dictPush as a parameter or jut simply push a new empty dictionary 
#in dictPush.  

def define(name, value):   
	'''Defines a name in the dict stack { 'name' : value }'''
	dictstack[currentDictionary][0][name] = value
	if name not in linksdict.keys():
		linksdict[name] = currentDictionary
	
'''
	d = dictPop()	
	print(d)
	d[0][name] = value
	print(d)
	dictPush(d[0],d[1])
'''

#add name:value to the top dictionary in the dictionary stack. (Keep the '/' in 
#name when you add it to the top dictionary) Your psDef function should pop the 
#name and value from operand stack and call the "define" function.    
def lookupOld(name):   
	'''Looks up a key in the dictionary stack starting at the top'''
	name = "/"+str(name)
	i = 0
	for d in dictstack:
		debug("dictionary " + str(i))
		i+=1
		debug(d)
		if name in d.keys():
			return d[name]
	return

def lookup(name, scope):   
	'''Looks up a key in the dictionary stack starting at the top'''
	name = "/"+str(name)
	i = 0
	#If the scope is dynamic then start at the top and go through the stack
		#Top to bottom
	newdictstack = dictstack[0:currentDictionary+1]	
	if scope == 'dynamic':
		for d in newdictstack[::-1]:
			debug("dictionary " + str(i))
			i+=1
			debug(d)
			if name in d[0].keys():
				return d[0][name]
	#If the scope is static then start at the top and follow the static links
		#the static links are stored in dictstack[i][1]
			#dictionaries are in dictstack[i][0]
	elif scope == 'static':
		#i = len(dictstack)-1
		i = currentDictionary
		loop = 0
		while i >= 0 and loop == 0:
			if i==0 and dictstack[i][1] == 0:
				loop+=1
			if name in dictstack[i][0].keys():
				return dictstack[i][0][name]
			else:
				i = dictstack[i][1]
				
	return

# return the value associated with name.   
# What is your design decision about what to do when there is no definition for 
# name? If name is not defined, your program should not break, but should  
# give an appropriate error message.   
#--------------------------- 15% -------------------------------------   
# Arithmetic and comparison operators:   define all the arithmetic and 
#comparison operators here --  add, sub, mul, div, eq, lt, gt     
#Make sure to check the operand stack has the correct number of parameters 
#and
#types of the parameters are correct.    
def checktoptwoint():
	'''Helper function to check if the top two values on the stack
		are either integers or floats'''
	if len(opstack)<2:
		print("Not enough values on the stack")
		return False
	op1 = opPop()
	op2 = opPop()
	if not isinstance(op1,int) and not isinstance(op1,float): 
		#lookup value in dictionary
		#if the value returned is not a digit
			#or is none, return
		opPush(op2)
		opPush(op1)
		print(str(op1) + " was not an integer")
		return False
	if not isinstance(op2,int) and not isinstance(op2,float): 
		#lookup value in dictionary
		#if the value returned is not a digit
			#or is none, return
		opPush(op2)
		opPush(op1)
		print(str(op2) + " was not an integer")
		return False
	opPush(op2)
	opPush(op1)
	return True
	
'''For all of these check the top two then push the operator of the two'''	
def add():
	if checktoptwoint():
		op2 = opPop()
		op1 = opPop()
		opPush(op1+op2)

def sub():
	if checktoptwoint():
		op2 = opPop()
		op1 = opPop()
		opPush(op1-op2)

def mul():
	if checktoptwoint():
		op2 = opPop()
		op1 = opPop()
		opPush(op1*op2)
	
def div():
	if checktoptwoint():
		op2 = opPop()
		op1 = opPop()
		opPush(op1/op2)

def eq():
	if checktoptwoint():
		op2 = opPop()
		op1 = opPop()
		opPush(op1==op2)

def lt():
	if checktoptwoint():
		op2 = opPop()
		op1 = opPop()
		opPush(op1<op2)

def gt():
	if checktoptwoint():
		op2 = opPop()
		op1 = opPop()
		opPush(op1>op2)

#--------------------------- 15% -------------------------------------   
# Array operators: define the array operators length, get 

def length():
	'''If the top element is a list then push the length of that list'''
	op1 = opPop()
	if isinstance(op1,list):
		opPush(len(op1))
	else:
		print("{0} is not a list".format(op1))
		opPush(op1)

def get():
	'''If the second element is a list and its length is not less than the 
		first element which must be an integer then return L[i]'''
	op1 = opPop()
	if not isinstance(op1,int):
		print("Cannot get non-integer index")
		opPush(op1)
		return
	op2 = opPop()
	if not isinstance(op2,list):
		print("Cannot index a non-list item")
		opPush(op2)
	if op1 > len(op2):
		print("Out of range index")
		opPush(op2)
		opPush(op1)
		return
	opPush(op2[op1])	

#--------------------------- 15% -------------------------------------   
# Boolean operators: define the boolean operators psAnd, psOr, psNot          
#Remember that these take boolean operands only. Anything else is an error    
def checktoptwobool():
	'''Checks to make sure both of the top two operators are both boolean values'''
	if len(opstack) < 2: print("Not enough elements in stack"); return False;
	op1 = opPop()
	op2 = opPop()
	if not isinstance(op1,bool) or not isinstance(op2,bool):
		print("Both operators must be boolean values")
		opPush(op2)
		opPush(op1)
		return False
	opPush(op2)
	opPush(op1)
	return True

'''For these just check the top two for bools then push the operator of the two'''
def psAnd():
	if not checktoptwobool():
		return 
	op1 = opPop()
	op2 = opPop()
	opPush(op1 and op2)

def psOr():
	if not checktoptwobool():
		return 
	op1 = opPop()
	op2 = opPop()
	opPush(op1 or op2)

def psNot():
	if len(opstack) < 1: print("Not enough elements in stack"); return;
	op1 = opPop()
	if not isinstance(op1,bool):
		print("Both operators must be boolean values")
		opPush(op1)
		return
	opPush(not op1)

#--------------------------- 25% -------------------------------------   
# Define the stack manipulation  and print operators:    dup, exch, pop, copy, 
#clear, stack   
def stack():
	'''prints values in the  stack'''
	print("CurrentScope = %s" %currentScope)
	print("CurrentDictionary = %d" %currentDictionary)
	print('========================')
	for i in opstack:
		print("|"+str(i)+"|")
	print('========================')
	i = len(dictstack)-1
	for t in dictstack[::-1]:
		print("---%d-----%d-----" %(i,t[1]))
		i-=1
		for key in t[0].keys():
			print("%s\t%s" %(key, t[0][key]))	
	print('========================')

def clear():
	'''Removes all values from the stack'''
	opstack.clear()

def pop():
	'''Pops a value from the stack if there is a value'''
	if len(opstack) < 1: print("Not enough elements in stack"); return;
	return opPop()

def dup():
	'''Duplicates the top element of the stack'''
	if len(opstack) < 1: print("Not enough elements in stack"); return;
	op1 = opPop()
	opPush(op1)
	opPush(op1)

def exch():
	'''Switches out the top two elements if they exist'''
	if len(opstack) < 2: print("Not enough elements in stack"); return;
	op1 = opPop()
	op2 = opPop()
	opPush(op1)
	opPush(op2)

def copy():
	'''Copies the top n values from the stack onto itself'''
	if len(opstack) < 1: print("Not enough elements in stack"); return;
	op1 = opPop()
	if not isinstance(op1,int):
		print ("Cannot copy non integer ammount of stack contents")
		opPush(op1)
		return
	elif op1 > len(opstack):
		print("Not enough elements in the stack")
		opPush(op1)
		return
	ops = []
	for i in range(0,op1):
		ops.insert(0,opPop())
	for i in ops:
		opPush(i)
	for i in ops:
		opPush(i)

#--------------------------- 20% -------------------------------------   
# Define the dictionary manipulation operators: psDict, begin, end, psDef   
# name the function for the def operator psDef because def is reserved in      
#Python.   

def psDict():
	'''Pushes an empty dictionary onto the stack'''
	opPush({})

def begin():
	'''Pushes the top dictionary on the stack or makes an empty dict and pushes that'''
	op1 = opPop()
	if isinstance(op1,dict):
		dictPush(op1)
	else: 
		dictPush()

def end():
	'''Removes the top dictionary from the stack if there is more than 1'''
	if len(dictstack) > 1:
		dictPop()   

def psDef():
	'''defines a value in the dictionary stack'''
	if len(opstack) < 2: print("Not enough elements in stack"); return;
	op1 = opPop()
	op2 = opPop()
	if not isinstance(op2,str):
		print("{0} is not a valid variable name".format(op2))
		opPush(op2)
		opPush(op1)
		return
	if op2[0] != '/':
		print("{0} is not a valid variable name".format(op2))
		opPush(op2)
		opPush(op1)
		return
	define(op2,op1)   

#-----------------------------------------------------------------------
# Defining code array functions: for forallm if, ifelse
#---------------------------------------------------------------------

def toptwolists():
	'''Checks to make sure both of the top two operators are both list values'''
	if len(opstack) < 2: print("Not enough elements in stack"); return False;
	op1 = opPop()
	op2 = opPop()
	if not isinstance(op1,list) or not isinstance(op2,list):
		print("Both operators must be list values")
		opPush(op2)
		opPush(op1)
		return False
	opPush(op2)
	opPush(op1)
	return True

def psFor():
	debug("For Stack vv")
	if debugFlag: stack()
	debug("^^^")
	code = opPop()
	if not isinstance(code,list):
		print("First operand must be code array")
		opPush(code)
		return
	if not checktoptwoint():
		return
	#end = opPop() #This one for real ps
	start = opPop() #this one for the TA code
	if not checktoptwoint():
		return
	step = opPop()
	#start = opPop() #This one for real ps
	end = opPop() #this one for the TA code
	debug('start = '+str(start)+' | step = '+str(step)+' | end = '+str(end) )

	if start < end:
		debug('here1')
		while(start <= end):
			opPush(start)
			debug('1.1) start = '+str(start)+' | step = '+str(step)+' | end = '+str(end) )
	#		print("l = %d | k = %d | j = %d" %(l,k,j))
			interpretSPS(code, currentScope, currentDictionary)
			start += step 
			debug('1.2) start = '+str(start)+' | step = '+str(step)+' | end = '+str(end) )
			#exit()
		return

	if start > end:
		debug('here2')
		while(start >= end):
			opPush(start)
			debug('2.1) start = '+str(start)+' | step = '+str(step)+' | end = '+str(end) )
	#		print("l = %d | k = %d | j = %d" %(l,k,j))
			interpret(code,currentScope,currentDictionary)
			start += step 
			debug('2.2) start = '+str(start)+' | step = '+str(step)+' | end = '+str(end) )
			#exit()
		return


def forall():
	if not toptwolists():
		return	
	code = opPop()
	arr = opPop()
	for val in arr:
		opPush(val)
		interpretSPS(code, currentScope, currentDictionary)

def Psif():
	code = opPop()
	if not isinstance(code,list):
		print('First operand must be code array')
		opPush(code)
		return
	bol = opPop()
	if not isinstance(bol,bool):
		print('Second operand must be bool')
		opPush(bol)
		opPush(code)
	if bol:
		interpretSPS(code, currentScope, currentDictionary)
	else:
		return


def ifelse():
	if not toptwolists():
		return	
	codeelse = opPop()
	codeif = opPop()
	bol = opPop()
	if not isinstance(bol,bool):
		debug('Second operand must be bool')
		opPush(bol)
		opPush(codeif)
		opPush(codeelse)
		return
	debug(bol)
	if bol:
		debug('executing ' + str(codeif))
		interpretSPS(codeif)
		return
	else:
		debug('executing ' + str(codeelse))
		interpretSPS(codeelse)
		return




# Note: The psDef operator will pop the value and name from the opstack and    
#call your own "define" operator (pass those values as parameters). Note that   
#psDef()won't have any parameters. 
def isbool(s):
	debug(s)
	if s == 'True': return True
	if s == 'true': return True
	if s == 'False': return False
	if s == 'false': return False
	return False 

def parseinput(cmd):
	inarr = False
	array = []
	cmds = cmd.split(' ')
	for v in cmds:
		if len(v) > 0:	
			if v == '[':
				inarr = True
			elif v == ']':
				inarr = False
				opPush(array)
			elif inarr:
				if v != '[':
					array.append(v)	
			elif v.isdigit():
				opPush(int(v))
			elif isbool(v) is not None:
				opPush(isbool(v))
			else:
				#if v[0] == '/':
				#	opPush(v)
				#	print("Defining variable " + v[1:])
				#else:
				#	print("Calling function " + v)
				if (v == 'quit'):
					sys.exit()
				try:
					globals()[v]()
				except:
					if lookup(v, scope) is None:
						if v[0] == '/':
							opPush(v)
						else:
							print("Command {0} not found".format(v))
							return
					else: opPush(lookup(v,scope))

def tokenize(s):
    return re.findall(
#"/?[a-zA-Z][a-zA-Z0-9_]*|[[][a-zA-Z0-9_\s!][a-zA-Z0-9_\s!]*[]]|[-]?[0-9]+|[}{]+|%.*|[^ \t\n]"
#               ,s)
"/?[a-zA-Z][a-zA-Z0-9_]*|[[][a-zA-Z0-9_\s!][a-zA-Z0-9_\s!]*[]]|[-]?[0-9]+|[}{]+|%.*|[^ \t\n]"
                ,s)


def groupMatching(it):
    res = []
    for c in it:
        if c == '}':
            return res
        elif c == '{':
            res.append(groupMatching(it))
        else:
            res.append(c)
    return False

def convlst(y):
    lst = []
    for i in y:
        lst.append(conv(i))
    return lst

def conv(y):
    '''
    Tries to convert to an int
            then a float
            then checks if it is 
                True/False
            then an array by checking
                if the first and last are []
                then iterates the list
                appending the converted value
    or just returns the value found
    '''
    debug('In convert with ' + str(y))
    if type(y) == list:
        return convlst(y)

    x = y
    try:
        x = int(x)
        return x
    except ValueError:
        try:
            x = float(x)
        except ValueError:
            if x == 'False' or x == 'false':
                x = False
                return x
            elif x == 'True' or x == 'true':
                x = True
                return x
            if x[0] is '[' and x[-1] is ']':
                lst = []
                for c in x[1:-1].split(' '):
                    lst.append(conv(c))
                return lst


    return x




def parse(L):
    res = []
    it = iter(L)
    for c in it:
        if c == '}':
            return False
        elif c =='{':
            res.append(groupMatching(it))
        else: res.append(c)

    #print('res vv')
    #print(res)
    #print('res ^^')
    res = list(map(conv,res))

    return res

def notint(i):
	return type(i) != int



functions = {'if':Psif,'def':psDef,'dict':psDict,'for':psFor }

def interpretSPS(code, scope, dictionary):
	global currentDictionary
	global currentScope
	currentDictionary = dictionary
	currentScope = scope
	i = 0
	debug(code)
	for element in code:
		try:
			globals()[element]()
		except Exception as e:
			if element == 'quit':
				sys.exit(0)
			debug('Error ' + str(e))
			try:
				functions[element]()
			except:
				if type(element) is str:
					if lookup(element,scope) is None:
						if str(element)[0] == '/':
							opPush(element)
						else:
							print("Command {0} not found".format(element))
							return
					else: 
						if type(lookup(element,scope)) == list:
							if len(list(filter(notint,lookup(element,scope)))) > 0:
								#dictPush({},dictionary)
								#dictPush({},0)
								dictPush({},linksdict['/'+str(element)]);
								interpretSPS(lookup(element,scope), scope, len(dictstack)-1)
								currentDictionary = dictionary
								currentScope = scope
								#dictPop()	
							else:
								opPush(lookup(element,scope))
						else:
							opPush(lookup(element,scope))
				else:
					opPush(element)
		debug('[%d] -> ' %i + str(element))	



def interpreter(s, scope):
	interpretSPS(parse(tokenize(s)),scope,0)

def unitTestSuite():
	cmd1 = '/square {dup mul} def [1 2 3 4] {square} forall add add add 30 eq true stack'
	cmd2 = '[1 2 3 4 5] dup length /n exch def /fact {0 dict begin /n exch def n 2 lt { 1} {n 1 sub fact n mul } ifelse end} def n fact stack'     
	cmd3 = '[9 9 8 4 10] {dup 5 lt {pop} if} forall stack'	
	cmd4 = '[1 2 3 4 5] dup length exch {dup mul}  forall add add add add exch 0 exch -1 1 {dup mul add} for eq stack' 
	print('========\nCmd1\n==========')	
	interpreter(cmd1)
	clear()
	print('========\nCmd2\n==========')	
	interpreter(cmd2)
	clear()
	print('========\nCmd3\n==========')	
	interpreter(cmd3)
	clear()
	print('========\nCmd4\n==========')	
	interpreter(cmd4)
	clear()
	print('========')



if __name__ == '__main__':
	#unitTestSuite()
	while True:
		command = input("PS<"+str(len(opstack))+">: ")
		if len(command) > 0:
			interpreter(command, 'dynamic')
			#parseinput(command)
		command = ''



