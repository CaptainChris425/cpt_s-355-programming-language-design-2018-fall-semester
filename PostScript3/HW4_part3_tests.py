'''
Christopher Young
Tests for postscript interpretor
'''


import re
from HW4_part2 import*
#global variables
opstack = []  #assuming top of the stack is the end of the list
dictstack = []  #assuming top of the stack is the end of the list


#------- Part 1 TEST CASES--------------
def testDefine():
    define("/y", 2)
    define("/r", 'hello')
    if lookup("y") != 2:
        return False
    if lookup("r") is not 'hello':
        return False
    return True

def testLookup():
    opPush("/p")
    opPush(5)
    psDef()
    if lookup("p") != 5:
        return False
    opPush("/v")
    opPush(0)
    psDef()
    if lookup("v") != 0:
        return False
    return True

#Arithmatic operator tests
def testAdd():
    opPush(1)
    opPush(2)
    add()
    if opPop() != 3:
        return False
    opPush(4)
    opPush(14)
    add()
    if opPop() != 18:
        return False
    return True

def testSub():
    opPush(10)
    opPush(4.5)
    sub()
    if opPop() != 5.5:
        return False
    opPush(-5)
    opPush(-6)
    sub()
    if opPop() != 1:
        return False
    return True

def testMul():
    opPush(2)
    opPush(4.5)
    mul()
    if opPop() != 9:
        return False
    opPush(5)
    opPush(5)
    mul()
    if opPop() != 25:
        return False
    return True

def testDiv():
    opPush(1)
    opPush(1)
    div()
    if opPop() != 1:
        return False
    opPush(100)
    opPush(40)
    div()
    if opPop() != 2.5:
        return False
    return True
    
#Comparison operators tests
def testEq():
    opPush(6)
    opPush(6)
    eq()
    if opPop() != True:
        return False
    opPush(True)
    opPush(True)
    eq()
    if opPop() != True:
        return False
    return True

def testLt():
    opPush(3)
    opPush(6)
    lt()
    if opPop() != True:
        return False
    opPush(5)
    opPush(3)
    lt()
    if opPop() != False:
        return False
    return True

def testGt():
    opPush(3)
    opPush(6)
    gt()
    if opPop() != False:
        return False
    opPush(5)
    opPush(3)
    gt()
    if opPop() != True:
        return False
    return True

#boolean operator tests
def testPsAnd():
    opPush(True)
    opPush(False)
    psAnd()
    if opPop() != False:
        return False
    opPush(True)
    opPush(True)
    psAnd()
    if opPop() != True:
        return False
    return True

def testPsOr():
    opPush(True)
    opPush(False)
    psOr()
    if opPop() != True:
        return False
    opPush(False)
    opPush(False)
    psOr()
    if opPop() != False:
        return False
    return True

def testPsNot():
    opPush(True)
    psNot()
    if opPop() != False:
        return False
    opPush(False)
    psNot()
    if opPop() != True:
        return False
    return True

#Array operator tests
def testLength():
    opPush([])
    length()
    if opPop() != 0:
        return False
    opPush([3,4,5])
    length()
    if opPop() != 3:
        return False
    opPush(['1','2','3'])
    length()
    if opPop() != 3:
        return False
    return True

def testGet():
    opPush([1,2,3,4,5])
    opPush(4)
    get()
    if opPop() != 5:
        return False
    opPush([1,2,3,4,5])
    opPush(0)
    get()
    if opPop() != 1:
        return False
    return True

#stack manipulation functions
def testDup():
    opPush(10)
    dup()
    if opPop()!=opPop():
        return False
    opPush(15)
    dup()
    if opPop()!=opPop():
        return False
    return True

def testExch():
    opPush(10)
    opPush("/x")
    exch()
    if opPop()!=10 and opPop()!="/x":
        return False
    opPush(80)
    opPush("1")
    exch()
    if opPop()!= 80 and opPop()!= "/x":
        return False
    return True

def testPop():
    l1 = len(opstack)
    opPush(10)
    pop()
    l2= len(opstack)
    if l1!=l2:
        return False
    l1 = len(opstack)
    opPush(1)
    pop()
    l2= len(opstack)
    if l1!=l2:
        return False
    return True

def testCopy():
    opPush(1)
    opPush(2)
    opPush(3)
    opPush(4)
    opPush(5)
    opPush(2)
    copy()
    if opPop()!=5 and opPop()!=4 and opPop()!=5 and opPop()!=4 and opPop()!=3 and opPop()!=2:
        return False
    opPush(3)
    opPush(1)
    opPush(1)
    opPush(2)
    if opPop()!=2 and opPop()!=1 and opPop()!=2 and opPop()!=1 and opPop()!=3: 
        return False
    return True

def testClear():
    opPush(10)
    opPush("/x")
    clear()
    if len(opstack)!=0:
        return False
    return True
    opPush(10)
    opPush("/x")
    opPush("/t")
    opPush("/g")
    opPush("/f")
    clear()
    if len(opstack)!=0:
        return False
    return True

#dictionary stack operators
def testDict():
    opPush(0)
    psDict()
    if opPop()!={}:
        return False
    return True

def testBeginEnd():
    opPush("/x")
    opPush(3)
    psDef()
    opPush({})
    begin()
    opPush("/x")
    opPush(4)
    psDef()
    if lookup("x")!=4:
        return False
    opPush({})
    begin()
    opPush("/x")
    opPush(5)
    psDef()
    if lookup("x")!=5:
        return False
    end()
    end()
    if lookup("x")!=3:
        return False
    return True

def testpsDef():
    opPush("/x")
    opPush(10)
    psDef()
    if lookup("x")!=10:
        return False
    opPush("/open")
    opPush(0)
    psDef()
    if lookup("open")!=0:
        return False
    return True

def testpsDef2():
    opPush("/x")
    opPush(10)
    psDef()
    opPush(1)
    psDict()
    begin()
    if lookup("x")!=10:
        end()
        return False
    end()
    return True

def testFor():
	line = '0 1 5 { } for'
	interpreter(line)
	op = opPop()
	if op != 5: 
		return False 
	op = opPop()
	if op != 4: 
		return False 
	clear()
	line = '1 0 1 5 {add} for'
	interpreter(line)
	op = opPop()
	if op != 16: 
		return False 
	clear()
	return True

def testForall():
	line = '[1 2 3 4] { } forall'
	interpreter(line)
	op = opPop()
	if op != 4: 
		return False 
	op = opPop()
	if op != 3: 
		return False 
	line = '[1 2 3 4] {dup mul} forall'
	interpreter(line)
	op = opPop()
	if op != 16: 
		return False 
	op = opPop()
	if op != 9: 
		return False 
	line = '[1 2 3 4] {3 lt} forall'
	interpreter(line)
	op = opPop()
	if op != False: 
		return False 
	op = opPop()
	op = opPop()
	if op != True: 
		return False 
	return True

def testIf():
	line = '1 2 3 lt {dup add} if' 
	interpreter(line)
	op = opPop()
	if op != 2: 
		return False 
	line = '3 True {[1 2 3 4] {add} forall} if' 
	interpreter(line)
	op = opPop()
	if op != 13: 
		return False 
	return True

def testIfelse():
	line = '1 2 3 gt {dup add} {3 mul} ifelse' 
	interpreter(line)
	op = opPop()
	if op != 3: 
		return False 
	line = '1 2 3 lt {dup add} {dup mul dup mul} ifelse' 
	interpreter(line)
	op = opPop()
	if op != 2: 
		return False 
	line = '2 False {dup add} {dup mul dup mul} ifelse' 
	interpreter(line)
	op = opPop()
	if op != 16: 
		return False 
	return True

def testCode():
	line = '/f {1 2 add} def f' 
	interpreter(line)
	op = opPop()
	if op != 3: 
		return False 
	line = '/f {1 2 add} def f f /g {add} def g' 
	interpreter(line)
	op = opPop()
	if op != 6: 
		return False 
	
	line = '/f {dup 5 lt {1 add f} { } ifelse} def 1 f' 
	interpreter(line)
	op = opPop()
	if op != 5: 
		return False 
	
	return True	

	
def main_part1():
    testCases = [('define',testDefine),('lookup',testLookup),('add', testAdd), ('sub', testSub),('mul', testMul),('div', testDiv), \
                 ('eq',testEq),('lt',testLt),('gt', testGt), ('psAnd', testPsAnd),('psOr', testPsOr),('psNot', testPsNot), \
                 ('length', testLength),('get', testGet), ('dup', testDup), ('exch', testExch), ('pop', testPop), ('copy', testCopy), \
                 ('clear', testClear), ('dict', testDict), ('begin', testBeginEnd), ('psDef', testpsDef), ('psDef2', testpsDef2), \
				 ('for',testFor), ('forall',testForall), ('if',testIf),('ifelse',testIfelse),('code',testCode)]
    # add you test functions to this list along with suitable names
    failedTests = [testName for (testName, testProc) in testCases if not testProc()]
    if failedTests:
        return ('Some tests failed', failedTests)
    else:
        return ('All tests OK')

if __name__ == '__main__':
    print(main_part1())
