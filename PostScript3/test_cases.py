from HW4_part2 import *

def main_part2():
    testcase1 = """
    /fact{
    0 dict
            begin
                    /n exch def
                    1
                    n -1 1 {mul} for
            end
    } def
    7
    fact
    stack
    """
    #[5040]

    testcase2 = """
    /n 6 def
    /fact {
         0 dict begin
            /n exch def
            n 2 lt { 1} {n 1 sub fact n mul } ifelse
         end } def
         n fact stack
    """
    #[720]

	#------call interpret functions------
    print("----Test Case 1----")
    interpreter(testcase1)
    # output should print [5040]
    clear()  # clear the stack for next test case

    print("----Test Case 2----")
    interpreter(testcase2)
    # output should be  [720]
    clear()  # clear the stack for next test case


if __name__ == '__main__':
	main_part2()
