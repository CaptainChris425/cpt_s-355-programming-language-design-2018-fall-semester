import re
from HW4_part3 import*
#global variables
opstack = []  #assuming top of the stack is the end of the list
dictstack = []  #assuming top of the stack is the end of the list

def testStatic():
	line = '/x 4 def /g { x } def /f { /x 7 def g } def f'
	interpreter(line,'static')
	op = opPop()
	clear()
	if op != 4:
		return False
	return True

def testStatic2():
	line = '/m 50 def /n 100 def /egg1 {/m 25 def n} def /chic { /n 1 def /egg2 {n} def m n egg1 egg2 } def n chic' 
	interpreter(line,'static')
	op = opPop()
	if op != 1:
		return False
	op = opPop()
	if op != 100:
		return False
	op = opPop()
	if op != 1:
		return False
	clear()
	return True


def testDynamic():
	line = '/x 4 def /g { x } def /f { /x 7 def g } def f'
	interpreter(line,'dynamic')
	op = opPop()
	clear()
	if op != 7:
		return False
	return True

def testDynamic2():
	line = '/m 50 def /n 100 def /egg1 {/m 25 def n} def /chic { /n 1 def /egg2 {n} def m n egg1 egg2 } def n chic' 
	interpreter(line,'dynamic')
	op = opPop()
	if op != 1:
		return False
	op = opPop()
	if op != 1:
		return False
	op = opPop()
	if op != 1:
		return False
	clear()
	return True

def main_part1():
    testCases = [('testStatic2',testStatic2), ('testDynamic2',testDynamic2), ('testStatic',testStatic), ('testDynamic',testDynamic)]
                 
                 
    # add you test functions to this list along with suitable names
    failedTests = [testName for (testName, testProc) in testCases if not testProc()]
    if failedTests:
        return ('Some tests failed', failedTests)
    else:
        return ('All part-1 tests OK')

if __name__ == '__main__':
    print(main_part1())
