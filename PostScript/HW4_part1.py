'''
Christopher Young
Python interpreter for Postscript like language
CptS 355
'''
import sys
#------------------------- 10% -------------------------------------   
# The operand stack: define the operand stack and its operations   
opstack = []   
# now define functions to push and pop values to the top of to/from the top of 
#the stack (end of the list). Recall that `pass` in Python is a space 
#holder: replace it with your code.   
def opPop():    
	'''pops the top value off if there is atleast one value'''
	if len(opstack) < 1:
		print("No values to pop")
		return
	return opstack.pop(0)

def opPush(value):   
	'''Pushes the value into the top of the stack'''
	opstack.insert(0,value)

# Remember that there is a Postscript operator called "pop" so we choose       
#different names for these functions.   
#-------------------------- 20% -------------------------------------   
# The dictionary stack: define the dictionary stack and its operations   
dictstack = [{}]   
# now define functions to push and pop dictionaries on the dictstack, to define
# name, and to lookup a name   
def dictPop():   
	'''pops the top dictionary off the stack'''
	return dictstack.pop(0)

# dictPop pops the top dictionary from the dictionary stack. 
def dictPush(dic = None):
	'''Pushes a dictionary onto the stack either passed in or an empty one is created'''
	if dic is not None and isinstance(dic, dict):
		dictstack.insert(0,dic)
	else:
		dictstack.insert(0,{})

#dictPush pushes a new dictionary to the dictstack. Note that, your interpreter 
#will call dictPush only when Postscript "begin" operator is called. "begin" 
#should pop the empty dictionary from the opstack and push it onto the dictstack 
#by calling dictPush. You may either pass this dictionary (which you popped from 
#opstack) to dictPush as a parameter or jut simply push a new empty dictionary 
#in dictPush.  

def define(name, value):   
	'''Defines a name in the dict stack { 'name' : value }'''
	d = dictPop()	
	d[name] = value
	dictPush(d)

#add name:value to the top dictionary in the dictionary stack. (Keep the '/' in 
#name when you add it to the top dictionary) Your psDef function should pop the 
#name and value from operand stack and call the "define" function.    
def lookup(name):   
	'''Looks up a key in the dictionary stack starting at the top'''
	name = "/"+name
	for d in dictstack:
		if name in d.keys():
			return d[name]
	return
# return the value associated with name.   
# What is your design decision about what to do when there is no definition for 
# name? If name is not defined, your program should not break, but should  
# give an appropriate error message.   
#--------------------------- 15% -------------------------------------   
# Arithmetic and comparison operators:   define all the arithmetic and 
#comparison operators here --  add, sub, mul, div, eq, lt, gt     
#Make sure to check the operand stack has the correct number of parameters 
#and
#types of the parameters are correct.    
def checktoptwoint():
	'''Helper function to check if the top two values on the stack
		are either integers or floats'''
	if len(opstack)<2:
		print("Not enough values on the stack")
		return False
	op1 = opPop()
	op2 = opPop()
	if not isinstance(op1,int) and not isinstance(op1,float): 
		#lookup value in dictionary
		#if the value returned is not a digit
			#or is none, return
		opPush(op2)
		opPush(op1)
		print(str(op1) + " was not an integer")
		return False
	if not isinstance(op2,int) and not isinstance(op2,float): 
		#lookup value in dictionary
		#if the value returned is not a digit
			#or is none, return
		opPush(op2)
		opPush(op1)
		print(str(op2) + " was not an integer")
		return False
	opPush(op2)
	opPush(op1)
	return True
	
'''For all of these check the top two then push the operator of the two'''	
def add():
	if checktoptwoint():
		op2 = opPop()
		op1 = opPop()
		opPush(op1+op2)

def sub():
	if checktoptwoint():
		op2 = opPop()
		op1 = opPop()
		opPush(op1-op2)

def mul():
	if checktoptwoint():
		op2 = opPop()
		op1 = opPop()
		opPush(op1*op2)
	
def div():
	if checktoptwoint():
		op2 = opPop()
		op1 = opPop()
		opPush(op1/op2)

def eq():
	if checktoptwoint():
		op2 = opPop()
		op1 = opPop()
		opPush(op1==op2)

def lt():
	if checktoptwoint():
		op2 = opPop()
		op1 = opPop()
		opPush(op1<op2)

def gt():
	if checktoptwoint():
		op2 = opPop()
		op1 = opPop()
		opPush(op1>op2)

#--------------------------- 15% -------------------------------------   
# Array operators: define the array operators length, get 

def length():
	'''If the top element is a list then push the length of that list'''
	op1 = opPop()
	if isinstance(op1,list):
		opPush(len(op1))
	else:
		print("{0} is not a list".format(op1))
		opPush(op1)

def get():
	'''If the second element is a list and its length is not less than the 
		first element which must be an integer then return L[i]'''
	op1 = opPop()
	if not isinstance(op1,int):
		print("Cannot get non-integer index")
		opPush(op1)
		return
	op2 = opPop()
	if not isinstance(op2,list):
		print("Cannot index a non-list item")
		opPush(op2)
	if op1 > len(op2):
		print("Out of range index")
		opPush(op2)
		opPush(op1)
		return
	opPush(op2[op1])	

#--------------------------- 15% -------------------------------------   
# Boolean operators: define the boolean operators psAnd, psOr, psNot          
#Remember that these take boolean operands only. Anything else is an error    
def checktoptwobool():
	'''Checks to make sure both of the top two operators are both boolean values'''
	if len(opstack) < 2: print("Not enough elements in stack"); return False;
	op1 = opPop()
	op2 = opPop()
	if not isinstance(op1,bool) or not isinstance(op2,bool):
		print("Both operators must be boolean values")
		opPush(op2)
		opPush(op1)
		return False
	opPush(op2)
	opPush(op1)
	return True

'''For these just check the top two for bools then push the operator of the two'''
def psAnd():
	if not checktoptwobool():
		return 
	op1 = opPop()
	op2 = opPop()
	opPush(op1 and op2)

def psOr():
	if not checktoptwobool():
		return 
	op1 = opPop()
	op2 = opPop()
	opPush(op1 or op2)

def psNot():
	if len(opstack) < 1: print("Not enough elements in stack"); return;
	op1 = opPop()
	if not isinstance(op1,bool):
		print("Both operators must be boolean values")
		opPush(op1)
		return
	opPush(not op1)

#--------------------------- 25% -------------------------------------   
# Define the stack manipulation  and print operators:    dup, exch, pop, copy, 
#clear, stack   
def stack():
	'''prints values in the stack'''
	for i in opstack:
		print("|"+str(i)+"|")

def clear():
	'''Removes all values from the stack'''
	opstack.clear()

def pop():
	'''Pops a value from the stack if there is a value'''
	if len(opstack) < 1: print("Not enough elements in stack"); return;
	return opPop()

def dup():
	'''Duplicates the top element of the stack'''
	if len(opstack) < 1: print("Not enough elements in stack"); return;
	op1 = opPop()
	opPush(op1)
	opPush(op1)

def exch():
	'''Switches out the top two elements if they exist'''
	if len(opstack) < 2: print("Not enough elements in stack"); return;
	op1 = opPop()
	op2 = opPop()
	opPush(op1)
	opPush(op2)

def copy():
	'''Copies the top n values from the stack onto itself'''
	if len(opstack) < 1: print("Not enough elements in stack"); return;
	op1 = opPop()
	if not isinstance(op1,int):
		print ("Cannot copy non integer ammount of stack contents")
		opPush(op1)
		return
	elif op1 > len(opstack):
		print("Not enough elements in the stack")
		opPush(op1)
		return
	ops = []
	for i in range(0,op1):
		ops.insert(0,opPop())
	for i in ops:
		opPush(i)
	for i in ops:
		opPush(i)

#--------------------------- 20% -------------------------------------   
# Define the dictionary manipulation operators: psDict, begin, end, psDef   
# name the function for the def operator psDef because def is reserved in      
#Python.   

def psDict():
	'''Pushes an empty dictionary onto the stack'''
	opPush({})

def begin():
	'''Pushes the top dictionary on the stack or makes an empty dict and pushes that'''
	op1 = opPop()
	if isinstance(op1,dict):
		dictPush(op1)
	else: 
		dictPush()

def end():
	'''Removes the top dictionary from the stack if there is more than 1'''
	if len(dictstack) > 1:
		dictPop()   

def psDef():
	'''defines a value in the dictionary stack'''
	if len(opstack) < 2: print("Not enough elements in stack"); return;
	op1 = opPop()
	op2 = opPop()
	if not isinstance(op2,str):
		print("{0} is not a valid variable name".format(op2))
		opPush(op2)
		opPush(op1)
		return
	if op2[0] != '/':
		print("{0} is not a valid variable name".format(op2))
		opPush(op2)
		opPush(op1)
		return
	define(op2,op1)   




# Note: The psDef operator will pop the value and name from the opstack and    
#call your own "define" operator (pass those values as parameters). Note that   
#psDef()won't have any parameters. 
def isbool(s):
	if s == 'True': return True
	if s == 'False': return False
	return None

def parseinput(cmd):
	inarr = False
	array = []
	cmds = cmd.split(' ')
	for v in cmds:
		if len(v) > 0:	
			if v == '[':
				inarr = True
			elif v == ']':
				inarr = False
				opPush(array)
			elif inarr:
				if v != '[':
					array.append(v)	
			elif v.isdigit():
				opPush(int(v))
			elif isbool(v) is not None:
				opPush(isbool(v))
			else:
				#if v[0] == '/':
				#	opPush(v)
				#	print("Defining variable " + v[1:])
				#else:
				#	print("Calling function " + v)
				if (v == 'quit'):
					sys.exit()
				try:
					globals()[v]()
				except:
					if lookup(v) is None:
						if v[0] == '/':
							opPush(v)
						else:
							print("Command {0} not found".format(v))
							return
					else: opPush(lookup(v))

if __name__ == '__main__':
	while True:
		command = input("PS<"+str(len(opstack))+">: ")
		if len(command) > 0:
			parseinput(command)
		command = ''



