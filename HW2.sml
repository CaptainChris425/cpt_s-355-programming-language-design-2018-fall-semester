(*Christopher Young
09/22/18
Homework 2 Standard ML*)
fun fold f base [] = base
	| fold f base (x::rest) = f x (fold f base rest);
fun filter pred [] = []
	| filter pred (x::rest) = if pred x then x::(filter pred rest)
								else (filter pred rest);

(*Question 1
	Making a function that will sum up the first n numbers of a list
		and outputs the numbers until they add up to over the sum*)
fun numbersToSum x lst= 
	let
		fun numsum x a [] = []
			|numsum x a (v::rest) = if ((v + a) < x) then v::(numsum x (v + a) rest) else (numsum x a []);
	in
		(numsum x 0 lst)
	end;

fun numbersToSumTail x lst =
	let
		fun numsum x a acc [] = acc 
			|numsum x a acc (v::rest) = if ((v + a) < x) then (numsum x (v + a) (v::acc) rest) else (numsum x a acc []);
	in
		rev(numsum x 0 [] lst) 
	end;

fun numbersToSumTest () =
	let 
		val t1 = if (numbersToSum 10 [1,2,3,4] = [1,2,3]) then true else false
		val t2 = if (numbersToSum 1 [9] = []) then true else false
		val t3 = if (numbersToSum 80 [1,2,3,4] = [1,2,3,4]) then true else false
	in
		print ("NumbersToSum: -------------------------\n  test1: " ^ Bool.toString(t1) ^ "\n" ^
				"  test2: " ^ Bool.toString(t2) ^ "\n" ^
				"  test3: " ^ Bool.toString(t3) ^ "\n")
	end

val _ = numbersToSumTest()

fun numbersToSumTailTest () =
	let 
		val t1 = if (numbersToSumTail 10 [1,2,3,4] = [1,2,3]) then true else false
		val t2 = if (numbersToSumTail 1 [9] = []) then true else false
		val t3 = if (numbersToSumTail 80 [1,2,3,4] = [1,2,3,4]) then true else false
	in
		print ("NumbersToSum: -------------------------\n  test1: " ^ Bool.toString(t1) ^ "\n" ^
				"  test2: " ^ Bool.toString(t2) ^ "\n" ^
				"  test3: " ^ Bool.toString(t3) ^ "\n")
	end

val _ = numbersToSumTailTest()
(*Question 2
	Make a partitioning function that will take a list and a bool function
		and for every value in the list if it passes the function
		then put it in the low partition, and if not then the high.
*)


fun partition f [] = ([],[])
	|partition f lst = (filter f lst, filter (fn x => not(f x)) lst);

fun partitionTest () =
	let 
		val t1 = (partition (fn x => (x<4)) [1,2,3,4,5,3,4,2]) = ([1,2,3,3,2],[4,5,4])
		val t2 = (partition (fn x => (x>4)) [1,2,3,4,5,3,4,2]) = ([5],[1,2,3,4,3,4,2])
		val t3 = (partition (fn x => (x=4)) [1,2,3,4,5,3,4,2]) = ([4,4],[1,2,3,5,3,2])
	in
		print ("Partition: -------------------------\n  test1: " ^ Bool.toString(t1) ^ "\n" ^
				"  test2: " ^ Bool.toString(t2) ^ "\n" ^
				"  test3: " ^ Bool.toString(t3) ^ "\n")
	end

val _ = partitionTest()

(*Question 3
	Make a function that will take in a list and return a bool
		true if all the elements are unique and false if there are duplicates
*)

fun areAllUnique lst = 
	let
		fun cil [] x = 0
			| cil (v::rest) x = if v = x then 1+(cil rest x) else 0+(cil rest x)
		(*true if there is more than 1 in the list*)
		fun noil lst x = if cil lst x = 1 then false else true 
		(*Gets a list of all the non unique elements in the list*)
		fun getul rest = filter (fn x => noil rest x) rest;
		(*Checks if the length of non-unique items in the list is 0*)
		fun unique lst = if length(getul lst) = 0 then true else false	
	in
		unique lst
	end;

fun areAllUniqueTest () =
	let 
		val t1 = areAllUnique [4,3,2,7,6,9] = true
		val t2 = areAllUnique [(1,2),(4,3),(8,9),(1,2)] = false
		val t3 = areAllUnique ["one","teo","nine","teo"] = false 
	in
		print ("areAllUnique: -------------------------\n  test1: " ^ Bool.toString(t1) ^ "\n" ^
				"  test2: " ^ Bool.toString(t2) ^ "\n" ^
				"  test3: " ^ Bool.toString(t3) ^ "\n")
	end

val _ = areAllUniqueTest()


(*Question 4
	A function that takes a list of lists and sums up all the values 
		and returns 1 int of the total sum*)
fun sum lst = 
	let
		fun add l = fold (fn x => fn y => x+y) 0 l;
		fun turntosum l = map (fn x => (add x)) l;
	in
		add (turntosum lst)
	end;

fun sumTest () =
	let 
		val t1 = sum [[1,2,3],[3,4],[1]] = 14
		val t2 = sum [[1],[1],[1],[1],[1]] = 5 
		val t3 = sum [[]] = 0 
	in
		print ("Sum: -------------------------\n  test1: " ^ Bool.toString(t1) ^ "\n" ^
				"  test2: " ^ Bool.toString(t2) ^ "\n" ^
				"  test3: " ^ Bool.toString(t3) ^ "\n")
	end

val _ = sumTest()

(*
fun sum lst = 
	let
		fun add l = fold (fn x => fn y => x+y) 0 l;
		fun addL acc [] = acc
			| addL acc (v::rest) = addL (acc + add v) rest
	in 
		addL 0 lst 
	end;
*)

(*Question 4.2
	Same as before but with option types*)
(*
fun sumOption SOME(lst) = 
	let
		fun add l = fold (fn x => fn y => SOME(x+y)) NONE (l);
		fun turntosum l = map (fn x => (add x)) l;
	in
		add (turntosum lst)
	end;
*)
(*Question 5
	Depth first scan of a polymorphic recursive tree where values are also stored on the
		nodes*)

datatype 'a Tree = LEAF of 'a | NODE of 'a * ('a Tree) * ('a Tree)


fun depthScan (LEAF(v)) = [v]
	| depthScan (NODE(v,t1,t2)) = (depthScan t1)@(depthScan t2)@[v];


(*Question 5.b
	Depth scan that returns the level of the first occurance of a value*)
(*
fun depthSearch t v = 
	let
		fun ds LEAF(s) v l = (s = v) 
			|ds (NODE(s,t1,t2)) v l = if (ds t1 v l+1) then l else (if (ds t2 v l+1) then l else (if (s = v) then l else -1));
	in
		ds t v 0;		
	end;


*)
